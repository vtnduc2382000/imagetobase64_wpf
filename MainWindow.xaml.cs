﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImageToBase64WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        string Base64Text;
        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image Files (* .BMP;* .JPG;* .PNG)|* .BMP;* .JPG;* .PNG" + "|All files (*.*)|*.*";
            dialog.CheckFileExists = true;
            dialog.Multiselect = false;
            if (dialog.ShowDialog() == true)
            {
                Bitmap image = new Bitmap(dialog.FileName);
                ingSource.Source = new BitmapImage(new Uri(dialog.FileName));
                byte[] ImageArray = System.IO.File.ReadAllBytes(dialog.FileName);
                Base64Text = Convert.ToBase64String(ImageArray);
                txtMutiplineText.Text = Base64Text;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            string path = "ImageToBase64.txt";
            using (StreamWriter stream = File.CreateText(path))
            {
                stream.Write(Base64Text);
            }
        }
    }
}
